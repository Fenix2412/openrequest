<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190622180143 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE idea_category_list (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(300) NOT NULL, available TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE idea_status_list (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, public TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE create_date create_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE idea ADD categories LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE short_description short_description VARCHAR(500) DEFAULT NULL, CHANGE description description VARCHAR(4000) DEFAULT NULL, CHANGE goals goals VARCHAR(4000) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE idea_category_list');
        $this->addSql('DROP TABLE idea_status_list');
        $this->addSql('ALTER TABLE idea DROP categories, CHANGE short_description short_description VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE goals goals VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE create_date create_date DATETIME DEFAULT \'NULL\'');
    }
}
