<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190622175606 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE idea (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(300) NOT NULL, short_description VARCHAR(500) DEFAULT NULL, description VARCHAR(4000) DEFAULT NULL, goals VARCHAR(4000) DEFAULT NULL, status SMALLINT NOT NULL, create_date DATETIME NOT NULL, modify_date DATETIME NOT NULL, creator_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE create_date create_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE idea');
        $this->addSql('ALTER TABLE user CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE create_date create_date DATETIME DEFAULT \'NULL\'');
    }
}
