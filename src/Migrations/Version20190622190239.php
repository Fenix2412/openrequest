<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190622190239 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL, CHANGE name name VARCHAR(180) NOT NULL, CHANGE email email VARCHAR(200) NOT NULL, CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE muted muted TINYINT(1) NOT NULL, CHANGE create_date create_date DATETIME NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495E237E06 ON user (name)');
        $this->addSql('ALTER TABLE idea CHANGE short_description short_description VARCHAR(500) DEFAULT NULL, CHANGE description description VARCHAR(4000) DEFAULT NULL, CHANGE goals goals VARCHAR(4000) DEFAULT NULL, CHANGE categories categories LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE idea CHANGE short_description short_description VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE goals goals VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE categories categories LONGTEXT DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\'');
        $this->addSql('DROP INDEX UNIQ_8D93D6495E237E06 ON user');
        $this->addSql('ALTER TABLE user DROP roles, CHANGE name name VARCHAR(150) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(150) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE create_date create_date DATETIME DEFAULT \'NULL\', CHANGE muted muted SMALLINT NOT NULL');
    }
}
