<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixture extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setName(uniqid('profil_'));

        $password = $this->passwordEncoder->encodePassword($user, 'Qwerty.1');

        $user->setPassword($password);
        $user->setEmail('dygulski13@gmail.com');
        $user->setCreateDate(new \DateTime('now'));
        $user->setStatus(1);
        $user->setMuted(false);

        $manager->persist($user);

        $manager->flush();
    }
}
