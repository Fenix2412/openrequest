<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    /**
     * @Route("/", name="main_index")
     */
    public function index()
    {
        $message = 'Welcome to the main page';
        $title = 'Welcome!';

        return $this->render('main/index.html.twig', [
            'title' => $title,
            'message' => $message
        ]);
    }
}
