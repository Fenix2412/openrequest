<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UserRegister;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Idea;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="user_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserRegister::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $user->setRoles(['ROLE_USER']);
            $user->setStatus(User::STATUS_ACTIVE);
            $user->setCreateDate(new \DateTime());
            $user->setMuted(false);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('main_index');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/hub", name="user_hub")
     */
    public function hub()
    {
        $ideaRepository = $this->getDoctrine()->getRepository(Idea::class);
        $user = $this->getUser();

        if(empty($user))
        {
            throw new HttpException(500, 'The application encountered a unexpected problem.');
        }

        $ideas = $ideaRepository->findBy([
            'creatorId' => $user->getId()
        ]);

        return $this->render('user/hub.html.twig', [
            'ideas' => $ideas
        ]); //TODO template
    }
}
