<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Idea;
use App\Form\IdeaForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IdeaController extends AbstractController
{

    /**
     * @Route("/idea/new", name="idea_new")
     */
    public function new(Request $request)
    {
        $idea = new Idea();
        $form = $this->createForm(IdeaForm::class, $idea);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idea = $form->getData();

            if($form->getClickedButton()->getName() === 'workingCopy')
            {
                $idea->setStatus(Idea::STATUS_WORKING_COPY);
            }
            else
            {
                $idea->setStatus(Idea::STATUS_SUBMITTED);
            }

            $idea->setCreateDate(new \DateTime());
            $idea->setModifyDate(new \DateTime());
            $idea->setCreatorId($this->getUser()->getId());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($idea);
            $entityManager->flush();

            return $this->redirectToRoute('user_hub');
        }

        return $this->render('idea/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/idea/edit/{id}", name="idea_edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $ideaRepository = $this->getDoctrine()->getRepository(Idea::class);
        $idea = $ideaRepository->findOneBy(array('id' => $id));

        if(empty($idea))
        {
            throw new HttpException(404, 'Idea not found');
        }

        if($idea->getCreatorId() != $this->getUser()->getId())
        {
            throw new HttpException(403, 'You are not allowed to edit ideas that are not yours');
        }

        $form = $this->createForm(IdeaForm::class, $idea);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idea = $form->getData();

            if($idea->getStatus() !== Idea::STATUS_SUBMITTED && $form->getClickedButton()->getName() === 'submit')
            {
                $idea->setStatus(Idea::STATUS_SUBMITTED);
            }
            $idea->setModifyDate(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($idea);
            $entityManager->flush();

            return $this->redirectToRoute('user_hub');
        }

        return $this->render('idea/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/idea/list/{page}", name="idea_list", requirements={"page"="\d+"})
     */
    public function list($page = 1)
    {
        $text = isset($_REQUEST['text']) ? $_REQUEST['text'] : '';
        $ideaRepository = $this->getDoctrine()->getRepository(Idea::class);
        $pagedPosts = $ideaRepository->getPagedPostsWithFilter($page, $text);
        $ideas = $pagedPosts->getIterator();
        $ideasCount = $ideas->count();

        if($ideasCount > 0)
        {
            $maxPages = ceil($pagedPosts->count() / $ideasCount);
        }
        else
        {
            $maxPages = 0;
        }

        return $this->render('idea/list.html.twig', [
            'ideas' => $ideas,
            'page' => $page,
            'maxPages' => $maxPages,
            'text' => $text
        ]);
    }
}
