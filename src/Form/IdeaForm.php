<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Idea;
use App\Entity\IdeaCategoryList;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class IdeaForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Title'])
            ->add('shortDescription', TextareaType::class, ['label' => 'Short description'])
            ->add('description', TextareaType::class, ['label' => 'Description'])
            ->add('goals', TextareaType::class, ['label' => 'Goals'])
            ->add('categories', EntityType::class, [
                'class' => IdeaCategoryList::class,
                'query_builder' => function (EntityRepository $er)
                {
                    return $er->createQueryBuilder('ic')
                        ->where('ic.available = 1');
                },
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $idea = $event->getData();
                $form = $event->getForm();

                if(empty($idea->getStatus()) || $idea->getStatus() === Idea::STATUS_WORKING_COPY)
                {
                    $form->add('workingCopy', SubmitType::class, [
                        'label' => 'Save a working copy',
                        'validation_groups' => 'working_copy'
                    ]);
                }
            })
            ->add('submit', SubmitType::class, [
                'label' => 'Submit your idea',
                'validation_groups' => 'submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Idea::class,
            //'validation_groups' => 'submit'
        ]);
    }
}
