<?php

namespace App\Repository;

use App\Entity\Idea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Idea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Idea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Idea[]    findAll()
 * @method Idea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Idea::class);
    }

    public function getPagedPostsWithFilter($page, $text)
    {
        $query = $this->createQueryBuilder('i')
            ->andWhere('i.status = :status')->setParameter('status', Idea::STATUS_SUBMITTED);
            //->andWhere('(i.title LIKE :text OR i.shortDescription LIKE :text)')->setParameter('text', '%'.$text.'%')
            //->getQuery();

        if(!empty($text))
        {
            $query->andWhere('(i.title LIKE :text OR i.shortDescription LIKE :text)')->setParameter('text', '%' . $text . '%');
        }

        $query = $query->getQuery();

        return $this->paginate($query, $page, 25);
    }

    public function getPagedPosts($page)
    {
        $query = $this->createQueryBuilder('i')
            ->where('i.status = :status')->setParameter('status', Idea::STATUS_SUBMITTED)
            ->getQuery();

        return $this->paginate($query, $page, 25);
    }

    private function paginate($query, $page, $itemCount = 15)
    {
        $paginator = new Paginator($query);

        $paginator->getQuery()
            ->setFirstResult($itemCount * ($page - 1))
            ->setMaxResults($itemCount);

        return $paginator;
    }

    // /**
    //  * @return Idea[] Returns an array of Idea objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Idea
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
