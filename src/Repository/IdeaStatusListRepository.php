<?php

namespace App\Repository;

use App\Entity\IdeaStatusList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IdeaStatusList|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdeaStatusList|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdeaStatusList[]    findAll()
 * @method IdeaStatusList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeaStatusListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IdeaStatusList::class);
    }

    // /**
    //  * @return IdeaStatusList[] Returns an array of IdeaStatusList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IdeaStatusList
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
